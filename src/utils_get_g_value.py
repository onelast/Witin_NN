import sys, os
sys.path.append(os.getcwd())
import numpy as np
import torch
from witin_nn import WitinConv2d, WitinLinear, WitinConvTranspose2d
import json
import model

def get_scale(q_bit, max):
    quat_max = 2**(q_bit-1)
    max = 2 ** torch.log2(max).ceil()
    assert max != 0, "Unable to determine quant scale, because max = 0"
    scale = quat_max / max
    return scale

def main(model_path, save_path):
    '''
    Args:
        model_path (str):  QAT/NAT训练完的浮点模型路径
        save_path (str):   转换后的定点模型保存路径

    Note:
        用户自行定义模型
    '''

    net = model.ResNet18()                                        #define your model
    net.load_state_dict(torch.load(model_path), strict=False)   #load your weights
    target_module = [WitinConvTranspose2d, WitinConv2d, WitinLinear]

    mo_list = []
    for tar_mo in target_module:
        for mo in net.modules():
            if isinstance(mo, tar_mo):
                mo_list.append(mo)

    print(mo_list)


    
    json_str = {}
    for mo in mo_list:
        layer_index = mo.layer_config.index
    
        weight_io_max = mo.autoscale_weight_obj.io_max
        x_io_max = mo.autoscale_x_obj.io_max
        y_io_max = mo.autoscale_y_obj.io_max
    
        weight_scale = get_scale(8, weight_io_max)
        x_scale = get_scale(8, x_io_max)
        y_scale = get_scale(8, y_io_max)
    
        G_value = x_scale * weight_scale / y_scale
        print(f'Layer index: {layer_index}, G_value: {G_value}')  # 添加调试信息
        json_str[str(layer_index)] = int(G_value.numpy())
    
    with open(save_path, 'w') as f:
        json.dump(json_str, f, indent=4)  # 使用 indent 参数格式化 JSON 输出

if __name__ == '__main__':
    model_path = 'model_params/witin/quan/ResNet18_param_4.pth'
    save_path = 'test.json'
    main(model_path, save_path)