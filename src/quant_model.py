import sys,os
project_root = os.path.abspath(os.path.join(os.path.dirname(__file__),'..'))
sys.path.append(project_root)
sys.path.append(r'../witin_nn/nn')

import torch
import torch.nn as nn
import witin_nn
from witin_nn.interface import ConfigFactory
from witin_nn import HandleNegInType

class ResidualBlock(nn.Module):
    def __init__(self, inchannel, outchannel, config, stride=1):
        super(ResidualBlock, self).__init__()
        self.config = config
        self.left = nn.Sequential(
            witin_nn.WitinConv2d(inchannel, outchannel, kernel_size=3, stride=stride, padding=1, bias=False, layer_config=self.config),
            witin_nn.WitinBatchNorm2d(outchannel, layer_config=self.config),
            witin_nn.WitinGELU(layer_config=self.config),
            witin_nn.WitinConv2d(outchannel, outchannel, kernel_size=3, stride=1, padding=1, bias=False, layer_config=self.config),
            witin_nn.WitinBatchNorm2d(outchannel, layer_config=self.config)
        )
        self.shortcut = nn.Sequential()
        if stride != 1 or inchannel != outchannel:
            self.shortcut = nn.Sequential(
                witin_nn.WitinConv2d(inchannel, outchannel, kernel_size=1, stride=stride, bias=False, layer_config=self.config),
                witin_nn.WitinBatchNorm2d(outchannel, layer_config=self.config)
            )
        self.gelu = witin_nn.WitinGELU(layer_config=self.config)
        self.add = witin_nn.WitinElementAdd(layer_config=self.config)

    def forward(self, x):
        out = self.left(x)
        shortcut = self.shortcut(x)
        out = self.add(out, shortcut)
        out = self.gelu(out)
        return out


class ResNet(nn.Module):
    def __init__(self, ResidualBlock, num_classes=10):
        super(ResNet, self).__init__()
        self.config_global = ConfigFactory.GlobalConfigFactory.get_dafault_config()
        self.config_global.use_quantization = True  # 使能量化
        self.config = ConfigFactory.LayerConfigFactory.get_default_config()
        self.config.use_quantization = True  # 使能量化
        self.config.w_clip = 127
        self.inchannel = 64

        self.conv1 = nn.Sequential(
            witin_nn.WitinConv2d(3, 64, kernel_size=3, stride=1, padding=1, bias=False, layer_config=self.config),
            witin_nn.WitinBatchNorm2d(64, layer_config=self.config),
            witin_nn.WitinGELU(layer_config=self.config)
        )
        self.layer1 = self.make_layer(ResidualBlock, 64, 2, stride=1)
        self.layer2 = self.make_layer(ResidualBlock, 128, 2, stride=2)
        self.layer3 = self.make_layer(ResidualBlock, 256, 2, stride=2)
        self.conv2 = nn.Sequential(
            witin_nn.WitinConv2d(256, 256, kernel_size=4, stride=4, bias=False, layer_config=self.config),
            witin_nn.WitinBatchNorm2d(256, layer_config=self.config),
            witin_nn.WitinGELU(layer_config=self.config)
        )
        self.dropout = nn.Dropout(0.2)
        self.fc = witin_nn.WitinLinear(1024, num_classes, layer_config=self.config)

    def make_layer(self, block, channels, num_blocks, stride):
        strides = [stride] + [1] * (num_blocks - 1)
        layers = []
        for stride in strides:
            layers.append(block(self.inchannel, channels, self.config, stride))
            self.inchannel = channels
        return nn.Sequential(*layers)

    def forward(self, x):
        out = self.conv1(x)
        out = self.layer1(out)
        out = self.layer2(out)
        out = self.layer3(out)
        out = self.conv2(out)
        out = self.dropout(out)
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        return out

def ResNet18():
    return ResNet(ResidualBlock)
