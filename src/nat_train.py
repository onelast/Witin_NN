import sys,os
project_root = os.path.abspath(os.path.join(os.path.dirname(__file__),'..'))
sys.path.append(project_root)
sys.path.append(r'../witin_nn/nn')

import torch
import torch.optim as optim
from matplotlib import pyplot as plt
from gzymodel import ResNet18
from train_fun import load_dataset, train_runner, test_runner, fig_plot


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
classes = ('plane', 'car', 'bird', 'cat', 'deer', 'dog', 'frog', 'horse', 'ship', 'truck')
model = ResNet18(nat=True).to(device)
file_path = '../model_params/witin/quan/ResNet18_param_' + '1' + '.pth'
model.load_state_dict(torch.load(file_path))
trainloader, testloader = load_dataset()
optimizer = optim.Adam(model.parameters(), lr=0.001)

epoch = 5
Loss = []
Accuracy = []
test_Loss = []
test_Accuracy = []

# config = [model.layer_config1, model.layer_config2, model.layer_config3, model.layer_config4, model.layer_config5, model.layer_config6]
# for cfg in config:
#     model.config_layer(cfg, use_quantization=True, scale_x=16, scale_y=16, scale_weight=16, bias_row_N=8, noise_level=0)
# # model.config_layer(use_quantization=True, scale_x=16, scale_y=16, scale_weight=16, bias_row_N=8, noise_level=0)


for epoch in range(0, epoch):
    # for name, param in model.named_parameters():
    #     print(f"Name: {name}, Parameter: {param.data}, Gradient: {param.grad}")
    directory = '../model_params/witin/quan'
    os.makedirs(directory, exist_ok=True)
    file_path = os.path.join(directory, 'ResNet18_param_' + str(epoch) + '.pth')
    loss, acc = train_runner(model, device, trainloader, optimizer, file_path, qat=False,nat=True,save_interval=10)
    print("\nepcoh: ", epoch)
    print("train: loss, acc", loss, acc)
    Loss.append(loss)
    Accuracy.append(acc)
    test_loss, test_acc = test_runner(model, device, testloader)
    test_Loss.append(test_loss)
    test_Accuracy.append(test_acc)
    print("test : loss, acc", test_loss, test_acc)

print('Finished Training')
# print('train_acc: ', Accuracy)
# print('test_acc: ', test_Accuracy)
fig_plot(Loss, test_Loss, Accuracy, test_Accuracy)