# 1.下载对应版本的witin-nn

下载并添加到项目中/witin_nn

版本为py3.8 linux

**注意：由于gitcode上传文件大小的闲置，这里没有将此目录提交，请自行下载，并存放在正确位置**

![image-20241228124049291](fig/image-20241228124049291.png)

## 2.安装并使用

witin nn还依赖一些包，已写在requirements.txt中，请先安装这些包。

<img src="fig/image-20241228131229070.png" alt="image-20241228131229070" style="zoom:67%;" />

以如图所示的目录结构为例，项目根目录中有src用于存放py代码，同时还有witin_nn是witin包的目录，需要导入witin_nn包时，可以参考src/enc_test.py在py文件的开头添加上系统的根目录，这样就能找到witin_nn包了。

```python
import sys,os
project_root = os.path.abspath(os.path.join(os.path.dirname(__file__),'..'))
sys.path.append(project_root)

print('Env test:\nPytorch:')
import torch
print(torch.cuda.is_available())

print('Witin:')
import witin_nn
```

