# Witin config参数与含义

## 一、layerconfig

| 名称                      | 数值               | 含义 |
|---------------------------|--------------------|------|
| **index**                 | 0                  | 层编号，应当每个层有唯一编号 |
| **G_algo**                | 1                  | ？gvalue么 |
| **w_clip**                | 127                | 闲置权重的范围 |
| **bias_row_N**            | 8                  | 计算bias所用的NPU行数，量化时使用，默认就行 |
| **target_platform**       | TargetPlatform.ANKE| 不管 |
| **hardware**              | HardwareType.ARRAY | 不管，array or vpu |
| **use_quantization**      | False              | 是否使用量化感知训练 |
| **noise_model**           | NoiseModel.NORMAL  | 噪声模型 |
| **noise_level**           | 0                  | 噪声强度 |
| **to_linear**             | False              | 是否将卷积层变成linear算子进行计算，训练时保持F，推理时呢？ |
| **scale_x**               | 1                  | 量化/浮点转换时输入的放大/缩小倍数 |
| **scale_y**               | 1                  | 输出的倍数 |
| **scale_weight**          | 1                  | 权重的倍数 |
| **handle_neg_in**         | HandleNegInType.FALSE | 对负输入的处理，仅量化感知训练时有效，值是HandelNegInType() |
| **x_quant_bits**          | 8                  | 量化的位数 |
| **y_quant_bits**          | 8                  | 量化的位数 |
| **bias_d**                | tensor(0)          | 拆出到数字计算的偏置，量化bit大于8时使用 |
| **conv2d_split_N**        | 1000000            | ？卷积层分割？不管 |
| **shift_num**             | 1.0                | **handle_neg_in**=shift时使用 |
| **output_fp_clip**        | None               | ？输出浮点数据的裁剪范围 |
| **debug_mode**            | False              | ？怎么用 |
| **use_auto_scale**        | True               | 自动计算x，y，weight的scale |
| **auto_scale_updata_step** | 2                  | 使用autoscale时更新scale的步数 |
| **cln_simple**            | False              | ？是否使用简化的计算方式？ |
| **mean_sum_quant_bits**   | 55                 | ？量化过程中求和均值的位宽，可能与数值精度相关 |
| **weight_quant_bits**     | 8                  | 权重量化的位数 |
| **scale_x_left**          | 1                  | ？输入数据缩放因子的左部分，可能用于定点数 |
| **scale_x_right**         | 1                  | ？同上 |
| **x_left_quant_bits**     | 8                  | ？左部分的量化位数 |
| **x_right_quant_bits**    | 8                  | ？右部分的量化位数 |
| **x_clip**                | None               | ？输入数据裁剪范围 |
| **sqrt_left_shift_bits**  | 8                  | ？平方根计算时的左移位数，可能用于调整进度 |
| **gelu_shift_num**        | None               | ？GELU激活函数相关的位移参数 |



## 二、global config

| 名称                   | 数值                  | 含义 |
| ---------------------- | --------------------- | ---- |
| target_platform        | TargetPlatform.ANKE   |      |
| noise_model            | NoiseModel.NORMAL     |      |
| noise_level            | 0                     |      |
| hardware               | HardwareType.ARRAY    |      |
| use_quantization       | False                 |      |
| use_to_linear          | False                 |      |
| w_clip                 | None                  |      |
| bias_row_N             | 8                     |      |
| handle_neg_in          | HandleNegInType.FALSE |      |
| x_quant_bits           | 8                     |      |
| y_quant_bits           | 8                     |      |
| bias_d                 | tensor(0)             |      |
| scale_x                | 1                     |      |
| scale_y                | 1                     |      |
| scale_weight           | 1                     |      |
| conv2d_split_N         | 1000000               |      |
| use_auto_scale         | True                  |      |
| auto_scale_updata_step | 2                     |      |
| shift_num              | 1.0                   |      |
| output_fp_clip         | None                  |      |
| debug_mode             | False                 |      |
| cln_simple             | False                 |      |
| mean_sum_quant_bits    | 55                    |      |
| weight_quant_bits      | 8                     |      |
| scale_x_left           | 1                     |      |
| scale_x_right          | 1                     |      |
| x_left_quant_bits      | 8                     |      |
| x_right_quant_bits     | 8                     |      |
| x_clip                 | None                  |      |
| sqrt_left_shift_bits   | 8                     |      |
| gelu_shift_num         | None                  |      |