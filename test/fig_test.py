import matplotlib.pyplot as plt
import numpy as np

# 模拟训练数据
epochs = 50
train_loss = np.random.uniform(0.1, 1.0, epochs)  # 模拟损失数据
test_loss = np.random.uniform(0.1, 1.0, epochs)
train_accuracy = np.random.uniform(0.7, 1.0, epochs)  # 模拟准确率数据
test_accuracy = np.random.uniform(0.7, 1.0, epochs)

# 绘制训练损失和准确率
def plot_and_save(train_loss, test_loss, train_accuracy, test_accuracy, epochs):
    plt.figure(figsize=(10, 5))

    # 绘制损失曲线
    plt.subplot(1, 2, 1)
    plt.plot(range(epochs), train_loss, label='Train Loss',color='#B0C4DE')
    plt.plot(range(epochs), test_loss, label='Test Loss', color='#8B008B')
    plt.title('Loss Curve')
    plt.xlabel('Epoch')
    plt.ylabel('Loss')
    plt.legend()

    # 绘制准确率曲线
    plt.subplot(1, 2, 2)
    plt.plot(range(epochs), train_accuracy, label='Train Accuracy')
    plt.plot(range(epochs), test_accuracy, label='Test Accuracy')
    plt.title('Accuracy Curve')
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.legend()

    # 保存图像
    plt.savefig('test_loss_accuracy_curve.png')
    print("Plot saved as 'test_loss_accuracy_curve.png'")
    
    # 关闭图像，释放内存
    plt.close()

# 调用绘图函数
plot_and_save(train_loss, test_loss, train_accuracy, test_accuracy, epochs)

