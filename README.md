

# Witin-NN based Image Clasification

Witin-NN赛题sth. went wrong队参赛项目

## 数据集

cifar10 http://www.cs.toronto.edu/~kriz/cifar.html

## 模型

ResNet-18

## 流程

参考官方文档

<img src="fig/image-20241228103950453.png" alt="image-20241228103950453" style="zoom:67%;" />



1. 环境搭建，pytorch、witin-NN，py版本使用3.8。（witin-NN安装方法？）
2. 模型pytorch版本，目标精度92
3. 模型Witin-NN，float版本，目标精度92
4. 训练后量化
5. 量化感知训练（QAT）
6. 存算model仿真测试（model在哪？）
7. 噪声感知训练（NAT）最终精度90以上
8. 芯片实测？？

